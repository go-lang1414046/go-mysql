package gomysql

import (
	"context"
	"fmt"
	"testing"
)

//don't need return data use execContext

func TestExecContext(t *testing.T) {
	db := GetConnection()
	defer db.Close()
	ctx := context.Background()

	script := "INSERT INTO customer(id,name) VALUES('1','Nurdin')"
	_, err := db.ExecContext(ctx, script)
	if err != nil {
		panic(err)
	}

	fmt.Println("Success insert new customer")
}

// need return data use querycontext
func TestQueryContext(t *testing.T) {
	db := GetConnection()
	defer db.Close()
	ctx := context.Background()

	script := "SELECT *FROM customer"
	rows, err := db.QueryContext(ctx, script)
	if err != nil {
		panic(err)
	}
	fmt.Println("Success insert new customer")

	defer rows.Close()
	for rows.Next() {
		var id, name string
		err := rows.Scan(&id, &name)
		if err != nil {
			panic(err)
		}
		fmt.Println("id", id)
		fmt.Println("name", name)
	}
}
